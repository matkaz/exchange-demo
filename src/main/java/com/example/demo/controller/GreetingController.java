package com.example.demo.controller;

import com.example.demo.model.Currencies;
import com.example.demo.model.Currency;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/greeting")
    public ResponseEntity<List<Currency>> greeting() {
        RestTemplate restTemplate = new RestTemplate();
        Currencies currencies = restTemplate.getForObject("http://webtask.future-processing.com:8068/currencies", Currencies.class);

        return new ResponseEntity<List<Currency>>(currencies.getItems(), HttpStatus.OK);
    }
}