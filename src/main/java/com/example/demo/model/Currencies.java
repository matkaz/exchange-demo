package com.example.demo.model;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class Currencies {

    private LocalDateTime publicationDate;
    private List<Currency> items;

}
