package com.example.demo.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "currency")
@Entity
public class Currency {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private String code;
    private Double unit;
    private Double purchasePrice;
    private Double sellPrice;
    private Double averagePrice;
    private Double value;

}